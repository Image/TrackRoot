#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
#Load libraries:

import numpy as np
import math as m
import io
import yaml
from scipy import signal as sg
import fnmatch
import os
from scipy.interpolate import UnivariateSpline
import matplotlib.pyplot as plt
from PIL import Image
from multiprocessing import Pool
from skimage import morphology

#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Input parameter loading:

## Load dictionnary:
with io.open('input_parameters.yaml', 'r') as infile:
    data_input = yaml.load(infile, Loader=yaml.FullLoader)

## Number of frame per second for movies:
fps_mov = data_input['displaying']['frame per second']
## Amplitude of the root tip direction vector:
amp_dir = data_input['displaying']['amplitude direction']
## Number of processor for post-processing parallelization:
nb_proc = data_input['computational']['number processor']
## Number of frame per second for movies:
fps_mov = data_input['displaying']['frame per second']
## Number of steps:
nb_stp = data_input['general']['number of step']
## Degree of the Chaikin's corner smoothening of the main root tip trajectory:
traj_smth = data_input['main root']['trajectory smooth']
## Main root tip speed smothening intensity:
main_spd_smth = data_input['main root']['trajectory smooth']
## Mask smoothing dimensions:
msk_smth = data_input['main root']['mask smooth']
## Root length smoothing intensity:
lgth_smth = data_input['main root']['length smooth']
## Targetted error for the polynomial fitting:
err_pol = data_input['main root']['error polynomial']
## Distance over wich to compute root tip direction:
d_dir = data_input['main root']['distance direction']


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Clean the tracking of the main root tip and interpolate speed

## Load the root tip data:
MatRoot = []
for itRt in range(len(fnmatch.filter(os.listdir('rootTip/byTip/'),'*.txt'))):
    MatRoot.append(np.loadtxt('rootTip/byTip/'+str(itRt).zfill(4)+'.txt'))

## Remove what is not the main root:
### Manual removing:
while True: 
    try:
        #### Get root tip coordinates:
        X0 = np.vstack(MatRoot)[:,0]
        Y0 = -np.vstack(MatRoot)[:,1]
        #### Plot them
        for iMrt in range(len(MatRoot)):    
            Xc = MatRoot[iMrt][:,0]; Yc=MatRoot[iMrt][:,1] 
            plt.plot(Xc,-Yc,'-',markersize=5,linewidth=2)
        
        #### Peak a part to remove: 
        plt.title('click on the part to remove \n or close')
        a = plt.ginput(1)
        plt.close()
        #### Detect the part to remove:
        d = np.sqrt((X0-a[0][0])**2.+(Y0-a[0][1])**2.)
        I = np.where(d==np.min(d))[0][0]
        flag1 = True; iMrt = 0
        while flag1:
            Xc = MatRoot[iMrt][:,0]; Yc=MatRoot[iMrt][:,1] 
            J = np.where((Xc==X0[I]).astype('int')*(Yc==-Y0[I]).astype('int'))[0]
            if len(J)>0:
                flag1 = False
            else:
                iMrt += 1
        
        #### Remove the selected part:
        del MatRoot[iMrt]
    
    except:
        break

## Concatenate and sort root detection:
mainRoot = np.vstack(MatRoot)
I = np.argsort(mainRoot[:,2])
mainRoot = mainRoot[I,]

## Remove double time:
### Initial polynom and time:
P = mainRoot[:,0:2]
T = mainRoot[:,2]
### Detect double time:
I = np.where((T[1:]-T[0:-1])==0)[0]
### Loop over double time to fix them:
while len(I)>0:
    itI = I[0]
    P[itI,0] = 0.5*(P[itI,0]+P[itI,0])
    P[itI,1] = 0.5*(P[itI,1]+P[itI,1])
    P = np.delete(P,itI+1,0)
    T = np.delete(T,itI+1)
    I = np.where((T[1:]-T[0:-1])==0)[0]

## Smooth root tip trajectory with Chaikin's corner cutting algorithm:
for NU in range(traj_smth):
    ### Compute new coordinates:
    Q = (3./4.)*P[0:-1,:]+(1./4.)*P[1:,:]
    R = (1./4.)*P[0:-1,:]+(3./4.)*P[1:,:] 
    Pn = np.zeros((2*len(P),2))
    Pn[np.array(range(1,len(Pn)-1,2))] = Q
    Pn[np.array(range(2,len(Pn),2))] = R
    Pn[0,:] = P[0,:]
    Pn[-1,:] = P[-1,:]
    ### Compute new time:
    T1 = (3./4.)*T[0:-1]+(1./4.)*T[1:]
    T2 = (1./4.)*T[0:-1]+(3./4.)*T[1:]
    Tn = np.zeros(2*len(P))
    Tn[np.array(range(1,len(Pn)-1,2))] = T1
    Tn[np.array(range(2,len(Pn),2))] = T2
    Tn[0] = T[0]
    Tn[-1] = T[-1]
    ### Rebuild:
    P = Pn
    T = Tn

#~ # !Debug!:
#~ P0 = mainRoot[:,0:2]
#~ T0 = mainRoot[:,2]
#~ plt.plot(P0[:,0],-P0[:,1],'-k')
#~ plt.plot(P[:,0],-P[:,1],'-b')
#~ plt.show()
#~ plt.close()
#~ plt.plot(T0,P0[:,1],'-k')
#~ plt.plot(T,P[:,1],'-b')
#~ plt.show()
#~ plt.close()

## Compute the speed from the smothed signal:
### Direct computation:
V = np.zeros(len(T))
V[1:] = np.sqrt((P[1:,0]-P[0:-1,0])**2.+(P[1:,1]-P[0:-1,1])**2.)/(T[1:]-T[0:-1])
### Smoothing:
Vn = np.convolve(V,np.ones(main_spd_smth)/float(main_spd_smth),mode='same')

#~ # !Debug!:
#~ plt.plot(mainRoot[:,2],mainRoot[:,3],'-ok')
#~ plt.plot(T,V,'-b')
#~ plt.plot(T,Vn,'-r')
#~ plt.show()
#~ plt.close()

## Plot result:
os.system('mkdir mainRoot')
os.system('mkdir mainRoot/rootTip')
plt.plot(T,Vn,'-k',linewidth=3)
plt.xlabel('time (min)')
plt.ylabel('tip speed (px/min)')
plt.savefig('mainRoot/rootTip/speedTip.png',dpi=400)
plt.savefig('mainRoot/rootTip/speedTip.svg')

## Saved data:
### Raw data:
np.savetxt('mainRoot/rootTip/position_raw.txt',mainRoot[:,0:2])
np.savetxt('mainRoot/rootTip/time_raw.txt',mainRoot[:,2])
### Smoothed data: 
np.savetxt('mainRoot/rootTip/position_smoothed.txt',P)
np.savetxt('mainRoot/rootTip/time_smoothed.txt',T)
np.savetxt('mainRoot/rootTip/speedTip_smoothed.txt',Vn)


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Skeleton of the main root:

## Compute root network skeleton:
### Make smothing filter:
smthFilt = np.zeros((msk_smth,msk_smth))
for itI in range(msk_smth):
    for itJ in range(msk_smth):
        dIJ = m.sqrt((itI-msk_smth/2.+0.5)**2.+(itJ-msk_smth/2.+0.5)**2.)
        if dIJ<msk_smth/2.:
            smthFilt[itI,itJ] = msk_smth/2.-dIJ

smthFilt = smthFilt/np.sum(smthFilt)
### Make storage folder:
os.system('mkdir pictureSkeleton/weightedSkeleton')

### Function to compute weigthed skeleton:
def GetWeightSkel(iPct): 
    print(str(iPct)+' / '+str(nb_stp))
    #### Load the mask:
    pict0 = np.array(Image.open('pictureSkeleton/mask/'+'%04d'%(iPct)+'.png'))/255.
    #### Smooth mask:
    pict1 = sg.convolve2d(pict0,smthFilt,mode='same')
    #### Re-binarize:
    pict2 = pict1>0.8
    #### Compute the medial axis:    
    pict3,pict4 = morphology.medial_axis(pict2,return_distance=True) ##Other choice: morphology.thin or morphology.skeletonize
    #### Store the weighted skeleton:
    im = Image.fromarray(pict3*pict4).convert('L')
    im.save('pictureSkeleton/weightedSkeleton/'+'%04d'%iPct+'.png')
    return 0

### Parallelization of the skeleton:
p = Pool(nb_proc)
null = p.map(GetWeightSkel,range(nb_stp))
p.close()

## Function to follow the main root:
def FollowMain(iPct):
    global It, Jt
    print(str(iPct)+' / '+str(nb_stp))
    ### Load the skeleton:
    pict0 = np.array(Image.open('pictureSkeleton/weightedSkeleton/'+'%04d'%(iPct)+'.png'))
    ### Make a mask:
    pict1 = (pict0>0).astype(int)
    
    ### Look for the tip:
    if iPct==0:
        #### If first picture take the lowest point:
        I_tmp,J_tmp = np.where(pict1>0.5)
        II = np.where(I_tmp==np.max(I_tmp))[0][0]
        It = I_tmp[II]
        Jt = J_tmp[II]
    else:
        ####Get skeleton ends:
        pict_end = pict1[1:-1,1:-1]*(pict1[0:-2,1:-1]+pict1[0:-2,0:-2]+pict1[0:-2,2:]+pict1[2:,1:-1]+pict1[2:,2:]+pict1[2:,0:-2]+pict1[1:-1,2:]+pict1[1:-1,0:-2])
        I,J = np.where(pict_end==1)
        I += 1
        J += 1
        
        #~ # !Debug!
        #~ plt.imshow(pict1)
        #~ plt.plot(J,I,'ob')
        #~ plt.show()
        #~ plt.close()
        
        ####Keep the closest from the previous position:
        d0 = np.sqrt((I-It)**2.+(J-Jt)**2.)
        II = np.where(d0==np.min(d0))[0][0]
        It = I[II]
        Jt = J[II]
        
        # !Debug!
        #~ plt.imshow(pict0)
        #~ plt.plot(Jt,It,'ob')
        #~ plt.show()
        #~ plt.close()
    
    ### Follow the root up:
    #### Initialization:
    vec_I = np.array([It])
    vec_J = np.array([Jt])
    Ic = It; Jc = Jt
    pict0[Ic,Jc] = 0
    #### Loop along the main root:
    while True:
        #### Get neighboor weight:
        vec_nei = np.array([pict0[Ic-1,Jc-1],pict0[Ic-1,Jc],pict0[Ic-1,Jc+1],pict0[Ic,Jc+1],
                            pict0[Ic+1,Jc+1],pict0[Ic+1,Jc],pict0[Ic+1,Jc-1],pict0[Ic,Jc-1]]) 
        #### Look for the largest is any:
        I = np.where(vec_nei>0)[0]
        if len(I)==0:
            break
        elif len(I)==1:
            if I[0]==0:
                Ic = Ic-1; Jc = Jc-1
            elif I[0]==1:
                Ic = Ic-1
            elif I[0]==2:
                Ic = Ic-1; Jc = Jc+1
            elif I[0]==3:
                Jc = Jc+1
            elif I[0]==4:
                Ic = Ic+1; Jc = Jc+1
            elif I[0]==5:
                Ic = Ic+1
            elif I[0]==6:
                Ic = Ic+1; Jc = Jc-1
            elif I[0]==7:
                Jc = Jc-1
        elif len(I)>1:
            II = np.argsort(vec_nei)
            I1 = II[-1]
            I2 = II[-2]
            if I1==0:
                Ic1 = Ic-1; Jc1 = Jc-1
            elif I1==1:
                Ic1 = Ic-1; Jc1 = Jc
            elif I1==2:
                Ic1 = Ic-1; Jc1 = Jc+1
            elif I1==3:
                Ic1 = Ic; Jc1 = Jc+1
            elif I1==4:
                Ic1 = Ic+1; Jc1 = Jc+1
            elif I1==5:
                Ic1 = Ic+1; Jc1 = Jc
            elif I1==6:
                Ic1 = Ic+1; Jc1 = Jc-1
            elif I1==7:
                Ic1 = Ic; Jc1 = Jc-1
            
            if I2==0:
                Ic2 = Ic-1; Jc2 = Jc-1
            elif I2==1:
                Ic2 = Ic-1; Jc2 = Jc
            elif I2==2:
                Ic2 = Ic-1; Jc2 = Jc+1
            elif I2==3:
                Ic2 = Ic; Jc2 = Jc+1
            elif I2==4:
                Ic2 = Ic+1; Jc2 = Jc+1
            elif I2==5:
                Ic2 = Ic+1; Jc2 = Jc
            elif I2==6:
                Ic2 = Ic+1; Jc2 = Jc-1
            elif I2==7:
                Ic2 = Ic; Jc2 = Jc-1
            
            pict01 = pict0
            pict01[Ic1,Jc1] = 0
            pict01[Ic2,Jc2] = 0
            vec_nei1 = np.array([pict01[Ic1-1,Jc1-1],pict01[Ic1-1,Jc1],pict01[Ic1-1,Jc1+1],pict01[Ic1,Jc1+1],
                                 pict01[Ic1+1,Jc1+1],pict01[Ic1+1,Jc1],pict01[Ic1+1,Jc1-1],pict01[Ic1,Jc1-1]])
            pict02 = pict0
            pict02[Ic2,Jc2] = 0
            pict02[Ic1,Jc1] = 0
            vec_nei2 = np.array([pict02[Ic2-1,Jc2-1],pict02[Ic2-1,Jc2],pict02[Ic2-1,Jc2+1],pict02[Ic2,Jc2+1],
                                 pict02[Ic2+1,Jc2+1],pict02[Ic2+1,Jc2],pict02[Ic2+1,Jc2-1],pict02[Ic2,Jc2-1]])
            
            if np.max(vec_nei1)>np.max(vec_nei2):
                Ic = Ic1; Jc = Jc1
            else:
                Ic = Ic2; Jc = Jc2
        
        vec_I = np.append(vec_I,Ic)
        vec_J = np.append(vec_J,Jc)
        pict0[Ic,Jc] = 0
        
        #~ # !Debug!
        #~ plt.imshow(pict0[Ic-10:Ic+10,Jc-10:Jc+10])
        #~ plt.plot(10,10,'or')
        #~ plt.show()
        #~ plt.close()
    
    ### Save the main root path:
    np.savetxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt',np.vstack((vec_I,vec_J)).T)


## Extract the main root skeleton:
### Make storage folder:
os.system('mkdir mainRoot/rootPath')
### Loop over pictures:
for iPct in range(nb_stp):
    FollowMain(iPct)

# !Debug!
# ~ cm=plt.cm.get_cmap('jet')
# ~ for iPct in range(nb_stp):
    # ~ IJ=np.loadtxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt')
    # ~ plt.plot(IJ[:,1],-IJ[:,0],'-',color=cm(float(iPct)/nb_stp))
    # ~ plt.text(IJ[0,1],-IJ[0,0],str(iPct),fontsize=10)

# ~ plt.show()
# ~ plt.close()

## Cut the not reliable space-time regions from the error diagram:
### Make the empty matrix: 
IJ0 = np.loadtxt('mainRoot/rootPath/'+str(0).zfill(4)+'.txt')
matErr = float('nan')*np.ones((nb_stp,np.loadtxt('mainRoot/rootPath/'+str(nb_stp-1).zfill(4)+'.txt').shape[0]))
### Loop over root path:
for iPct in range(nb_stp):
    #### Load the current path
    I0 = IJ0[:,0]; J0 = IJ0[:,1] 
    IJc = np.loadtxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt')
    Ic = IJc[:,0]; Jc = IJc[:,1]
    #### Align paths from the tip: 
    d0 = np.sqrt((I0[0]-Ic)**2.+(J0[0]-Jc)**2.)
    II = np.where(d0==np.min(d0))[0][0]
    Ic = Ic[II:]; Jc=Jc[II:]
    m0 = min(len(J0),len(Jc),matErr.shape[1])
    #### Compute the local distance:
    matErr[iPct,0:m0] = np.sqrt((I0[0:m0]-Ic[0:m0])**2.+(J0[0:m0]-Jc[0:m0])**2.)
    #### Update the path:
    IJ0 = IJc

### Select the point above which to cut:
plt.imshow(matErr,vmin=0.5*np.nanmean(matErr),vmax=1.5*np.nanmean(matErr),cmap=plt.cm.get_cmap('jet'),aspect='auto')
plt.colorbar()
plt.title('local remoteness \n click to select the highest (in space) point')
plt.xlabel('distance from the tip [px]')
plt.ylabel('time step')
a = plt.ginput(1,timeout=-1)
plt.close()
S_max = int(a[0][0])

## Fix pathes:
### Loop over root path:
for iPct in range(1,nb_stp):
    #### Load the previous path:
    IJp = np.loadtxt('mainRoot/rootPath/'+str(iPct-1).zfill(4)+'.txt')
    #### Load the current path:
    IJc = np.loadtxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt')
    if IJc.shape[0]<S_max:
        ##### If the path is too short:
        if IJc.shape[0]<(0.9*IJp.shape[0]):
            IJc = IJp
            print('Problem with path: '+str(iPct))
    else:
        ##### If the path is larger than the limit:
        ###### Cut the wrong part:
        IJc = IJc[0:S_max,:]
        ###### Add the one from the prvious path:
        d0 = np.sqrt((IJc[-1,0]-IJp[:,0])**2.+(IJc[-1,1]-IJp[:,1])**2.)
        II = np.where(d0==np.min(d0))[0][0]
        IJc = np.vstack((IJc,IJp[II:-1,:]))
    
    #### Save corrected path:
    np.savetxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt',IJc)

## Display main root evolution:
### Select the part to display: 
for iPct in range(nb_stp):
    IJ = np.loadtxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt')
    plt.plot(IJ[:,1],-IJ[:,0],'k-')

plt.title('click on lowest part to remove')
a = plt.ginput(1,timeout=-1)
plt.close()
Imax =- int(a[0][1])
### Display with time:
time0 = np.loadtxt('time[min].txt')
cm = plt.cm.get_cmap('jet')
for iPct in range(nb_stp):
    IJ = np.loadtxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt')
    II = np.where(IJ[:,0]>Imax)[0]
    flt_col = time0[iPct]/np.max(time0)
    plt.plot(IJ[II,1],-IJ[II,0],'-',linewidth=1.5,color=cm(flt_col))

sc = plt.scatter(np.zeros(len(time0)),np.zeros(len(time0)),c=time0,s=0,cmap=cm)
plt.colorbar(sc)
plt.axis('equal')
plt.xlabel('px')
plt.ylabel('px')
plt.savefig('mainRoot/mainRoot.png',dpi=400)
plt.savefig('mainRoot/mainRoot.svg')
plt.show()
plt.close()


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Properties of the main root skeleton:

## Computation of the main root length evolution:
### Initialize length vector:
vec_L = float('nan')*np.ones(len(time0))
### Loop over frames:
for iPct in range(len(vec_L)):
    #### Load root path coordinates:
    if os.path.isfile('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt'):
        IJ = np.loadtxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt')
        II = np.where(IJ[:,0]>Imax)[0]
        I0 = IJ[II,0]
        J0 = IJ[II,1]
        #### Smooth data:
        I1 = np.convolve(I0,np.ones(lgth_smth)/float(lgth_smth),mode='same'); I1[0:int(lgth_smth/2.)] = I0[0:int(lgth_smth/2.)]; I1[-int(lgth_smth/2.):] = I0[-int(lgth_smth/2.):]
        J1 = np.convolve(J0,np.ones(lgth_smth)/float(lgth_smth),mode='same'); J1[0:int(lgth_smth/2.)] = J0[0:int(lgth_smth/2.)]; J1[-int(lgth_smth/2.):] = J0[-int(lgth_smth/2.):]
        
        #~ # !Debug!
        #~ plt.plot(J0,-I0,'-k')
        #~ plt.plot(J1,-I1,'-b')
        #~ plt.show()
        #~ plt.close()
        
        #### Compute the length:
        vec_L[iPct]=np.sum(np.sqrt((I1[1:]-I1[0:-1])**2.+(J1[1:]-J1[0:-1])**2.))

### Display the evolution of the main root length:
plt.plot(time0,vec_L,'-k',linewidth=3)
plt.xlabel('time (min)')
plt.ylabel('main root length (px)')
plt.savefig('mainRoot/mainRootLength.png',dpi=400)
plt.savefig('mainRoot/mainRootLength.svg')
plt.show()
plt.close()
### Save data:
np.savetxt('mainRoot/length_raw.txt',vec_L)
np.savetxt('mainRoot/time.txt',time0)

## Computation of the main root growth evolution:
### Soomth main root length:
vec_L_smth = np.convolve(vec_L,np.ones(lgth_smth)/float(lgth_smth),mode='same'); vec_L_smth[0:int(lgth_smth/2.)] = vec_L[0:int(lgth_smth/2.)]; vec_L_smth[-int(lgth_smth/2.):] = vec_L[-int(lgth_smth/2.):]
np.savetxt('mainRoot/length_smooth.txt',vec_L_smth)
### Derive signal:
vec_V = np.zeros(len(vec_L))
vec_V[1:] = (vec_L_smth[1:]-vec_L_smth[0:-1])/(time0[1:]-time0[0:-1])
### Display the evolution of the main root length:
plt.plot(time0,vec_V,'-k',linewidth=3)
plt.xlabel('time (min)')
plt.ylabel('main root growth speed (px/min)')
plt.savefig('mainRoot/mainRootLengthSpeed.png',dpi=400)
plt.savefig('mainRoot/mainRootLengthSpeed.svg')
plt.show()
plt.close()
###Save data:
np.savetxt('mainRoot/speedLength.txt',vec_V)

## Computation of the main root curvature and orientation evolution:
### Function to compute curvature and local orientation:
def GetCurvature(iPct):
    print(str(iPct)+' / '+str(nb_stp))
    #### Load root path coordinates:
    if os.path.isfile('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt'):
        IJ = np.loadtxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt')
        II = np.where(IJ[:,0]>Imax)[0]
        I0 = IJ[II,0]
        J0 = IJ[II,1]
        #### Compute the curvature:
        ##### Get curvilinear coordinates:
        s = np.arange(len(I0))
        ##### Polynomial smooth of data:
        std = err_pol*np.ones_like(I0)
        I1 = UnivariateSpline(s,I0,k=4,w=1./np.sqrt(std))
        J1 = UnivariateSpline(s,J0,k=4,w=1./np.sqrt(std))
        
        #~ # !Debug!
        #~ plt.plot(J0,-I0,'-k')
        #~ plt.plot(J1(s),-I1(s),'-b')
        #~ plt.show()
        #~ plt.close()
        
        ##### Curvature computation: 
        Ip = I1.derivative(1)(s)
        Ipp = I1.derivative(2)(s)
        Jp = J1.derivative(1)(s)
        Jpp = J1.derivative(2)(s)
        C0 = (Jp*Ipp-Ip*Jpp)/np.power(Jp**2.+Ip**2.,3./2.)
        
        #~ # !Debug!
        #~ plt.scatter(J0,I0,c=C0,s=1,cmap=plt.cm.get_cmap('jet'))
        #~ plt.show()
        #~ plt.close()
        
        #### Store curvature:
        np.savetxt('mainRoot/rootCurvature/'+str(iPct).zfill(4)+'.txt',C0)
        
        #### Local orientation computation:
        O0 = np.arctan2(J1(s[1:])-J1(s[0:-1]),-I1(s[1:])+I1(s[0:-1]))*180./m.pi
        O0 = np.append(O0,O0[-1])
        
        #### Store curvature:
        np.savetxt('mainRoot/rootOrientation/'+str(iPct).zfill(4)+'.txt',O0)

### Make storage folder:
os.system('mkdir mainRoot/rootCurvature')
os.system('mkdir mainRoot/rootOrientation')
### Parallelization of the main root curvature computation:
p = Pool(nb_proc)
null = p.map(GetCurvature,range(nb_stp))
p.close()

### Display the curvature:
#### Color amplitude:
N0 = 0
for it in range(nb_stp):
    n0 = len(np.loadtxt('mainRoot/rootCurvature/'+str(it).zfill(4)+'.txt'))
    if N0<n0:
        N0 = n0

C0 = np.loadtxt('mainRoot/rootCurvature/'+str(nb_stp-1).zfill(4)+'.txt')
C1 = C0[0:int(0.3*len(C0))]
Cm = np.min(C1)
CM = np.max(C1)
#### In matrix form:
matDisp = float('nan')*np.ones((len(time0),N0))
for iPct in range(nb_stp):
    if os.path.isfile('mainRoot/rootCurvature/'+str(iPct).zfill(4)+'.txt'):
        C0 = np.loadtxt('mainRoot/rootCurvature/'+str(iPct).zfill(4)+'.txt')
        C0 = C0[np.arange(len(C0)-1,0,-1)]
        matDisp[-1-iPct,0:len(C0)] = C0

fig, ax = plt.subplots()
plt.imshow(matDisp,vmin=Cm,vmax=CM,cmap=plt.cm.get_cmap('jet'),aspect='auto')
plt.colorbar()
plt.title('root curvature (1/px)')
plt.xlabel('curvilinear coordinate (px)')
plt.ylabel('time (min)')
ax.set_yticks([0,0.2*matDisp.shape[0],0.4*matDisp.shape[0],0.6*matDisp.shape[0],0.8*matDisp.shape[0],matDisp.shape[0]])
ax.set_yticklabels([str(int(time0[-1])),str(int(time0[int(len(time0)*0.8)])),str(int(time0[int(len(time0)*0.6)])),str(int(time0[int(len(time0)*0.4)])),str(int(time0[int(len(time0)*0.2)])),'0'])
plt.savefig('mainRoot/mainRootCurvature1.png',dpi=400)
plt.savefig('mainRoot/mainRootCurvature1.svg')
plt.show()
plt.close()
### Along the root:
for iPct in range(nb_stp):
    IJ = np.loadtxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt')
    II = np.where(IJ[:,0]>Imax)[0]
    I0 = -IJ[II,0]
    J0 = IJ[II,1]
    C0 = np.abs(np.loadtxt('mainRoot/rootCurvature/'+str(iPct).zfill(4)+'.txt'))
    if iPct==0:
        sc = plt.scatter(J0,I0,c=C0,s=0.03,cmap=plt.cm.get_cmap('jet'),vmin=0,vmax=max(CM,-Cm))
        plt.colorbar(sc)
    else:
        plt.scatter(J0,I0,c=C0,s=0.03,cmap=plt.cm.get_cmap('jet'),vmin=0,vmax=max(CM,-Cm))

plt.title('root curvature magnitude (1/px)')
plt.axis('equal')
plt.xlabel('px')
plt.ylabel('px')
plt.savefig('mainRoot/mainRootCurvature2.png',dpi=400)
#~ plt.savefig('mainRoot/mainRootCurvature2.svg')
plt.show()
plt.close()

### Display the local orientation:
#### Color amplitude:
N0 = 0
for it in range(nb_stp):
    n0 = len(np.loadtxt('mainRoot/rootOrientation/'+str(it).zfill(4)+'.txt'))
    if N0<n0:
        N0 = n0

#### In matrix form:
matDisp = float('nan')*np.ones((len(time0),N0))
for iPct in range(nb_stp):
    if os.path.isfile('mainRoot/rootOrientation/'+str(iPct).zfill(4)+'.txt'):
        O0 = np.loadtxt('mainRoot/rootOrientation/'+str(iPct).zfill(4)+'.txt')
        O0 = O0[np.arange(len(O0)-1,0,-1)]
        matDisp[-1-iPct,0:len(O0)] = O0

fig, ax = plt.subplots()
plt.imshow(matDisp,vmin=np.nanmin(matDisp),vmax=np.nanmax(matDisp),cmap=plt.cm.get_cmap('jet'),aspect='auto')
plt.colorbar()
plt.title('root orientation (degree)')
plt.xlabel('curvilinear coordinate (px)')
plt.ylabel('time (min)')
ax.set_yticks([0,0.2*matDisp.shape[0],0.4*matDisp.shape[0],0.6*matDisp.shape[0],0.8*matDisp.shape[0],matDisp.shape[0]])
ax.set_yticklabels([str(int(time0[-1])),str(int(time0[int(len(time0)*0.8)])),str(int(time0[int(len(time0)*0.6)])),str(int(time0[int(len(time0)*0.4)])),str(int(time0[int(len(time0)*0.2)])),'0'])
plt.savefig('mainRoot/mainRootOrientation1.png',dpi=400)
plt.savefig('mainRoot/mainRootOrientation1.svg')
plt.show()
plt.close()
### Along the root:
for iPct in range(nb_stp):
    IJ = np.loadtxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt')
    II = np.where(IJ[:,0]>Imax)[0]
    I0 = -IJ[II,0]
    J0 = IJ[II,1]
    O0 = np.loadtxt('mainRoot/rootOrientation/'+str(iPct).zfill(4)+'.txt')
    if iPct==0:
        sc = plt.scatter(J0,I0,c=O0,s=0.03,cmap=plt.cm.get_cmap('jet'),vmin=np.nanmin(matDisp),vmax=np.nanmax(matDisp))
        plt.colorbar(sc)
    else:
        plt.scatter(J0,I0,c=O0,s=0.03,cmap=plt.cm.get_cmap('jet'),vmin=np.nanmin(matDisp),vmax=np.nanmax(matDisp))

plt.title('root orientation (degree)')
plt.axis('equal')
plt.xlabel('px')
plt.ylabel('px')
plt.savefig('mainRoot/mainRootOrientation2.png',dpi=400)
#~ plt.savefig('mainRoot/mainRootOrientation2.svg')
plt.show()
plt.close()

## Computation of the main root tortuosity evolution:
### Make storage vectors:
vec_Tor1 = np.zeros(len(vec_L))
vec_Tor2 = np.zeros(len(vec_L))
###Loop over pictures:
for iPct in range(nb_stp):
    ##Load data:
    IJ = np.loadtxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt')
    II = np.where(IJ[:,0]>Imax)[0]
    I0 = -IJ[II,0]
    J0 = IJ[II,1]
    C0 = np.abs(np.loadtxt('mainRoot/rootCurvature/'+str(iPct).zfill(4)+'.txt'))
    ##First definition: C/L
    vec_Tor1[iPct] = vec_L[iPct]/m.sqrt((I0[-1]-I0[0])**2.+(J0[-1]-J0[0])**2.)
    ##First definition: sum(abs(curv))/C
    vec_Tor2[iPct] = np.sum(C0)/vec_L[iPct]

###Display the evolution of the main root tortuosities:
plt.plot(time0,vec_Tor1,'-k',linewidth=3)
plt.xlabel('time (min)')
plt.ylabel('main root tortuosity 1')
plt.savefig('mainRoot/mainRootTortuosity1.png',dpi=400)
plt.savefig('mainRoot/mainRootTortuosity1.svg')
plt.show()
plt.close()
plt.plot(time0,vec_Tor2,'-k',linewidth=3)
plt.xlabel('time (min)')
plt.ylabel('main root tortuosity 2')
plt.savefig('mainRoot/mainRootTortuosity2.png',dpi=400)
plt.savefig('mainRoot/mainRootTortuosity2.svg')
plt.show()
plt.close()
###Save data:
np.savetxt('mainRoot/tortuosity1.txt',vec_Tor1)
np.savetxt('mainRoot/tortuosity2.txt',vec_Tor2)


## Computation of the main root fractal dimension evolution:
### Function to compute the fractal dimension of the main root:
def FractalDim(iPct):
    global Imax
    print(str(iPct)+' / '+str(nb_stp))
    #### Load the skeleton:
    pict0 = np.array(Image.open('pictureSkeleton/weightedSkeleton/'+'%04d'%(iPct)+'.png'))
    #### Make a mask:
    pict1 = 255*(pict0>0)
    #### Remove useless parts:
    I,J = np.where(pict1>0)
    pict2 = pict1[Imax:np.max(I),np.min(J):np.max(J)].astype('uint8')
    #### Compute fractal dimension:
    ##### Compute the square size vs. number:
    C = np.unique(np.logspace(0,m.log10(np.min(pict2.shape)),20).astype(int))
    S = np.zeros(len(C))
    for itC in range(len(C)):
        pict3 = Image.fromarray(pict2)
        pict3.thumbnail((int(pict2.shape[0]/C[itC]),int(pict2.shape[1]/C[itC])),Image.ANTIALIAS)
        pict4 = np.array(pict3)>(np.max(pict3)*0.5)
        
        #~ # !Debug!
        #~ plt.imshow(pict4)
        #~ plt.show()
        #~ plt.close()
        
        S[itC] = np.sum(pict4)
    
    ##### Fit the fractal dimension:
    I = np.where(S>0)
    p0 = np.polyfit(np.log10(C[I]),np.log10(S[I]),1)
    
    # !Debug!
    #~ plt.plot(C[I],S[I],'ok')
    #~ plt.plot(C[I],10.**p[1]*C[I]**p[0],'-b')
    #~ plt.xscale('log')
    #~ plt.yscale('log')
    #~ plt.show()
    #~ plt.close()
    
    return p0[0]

### Parallelization of the main root fractal dimension computation:
p = Pool(nb_proc)
vec_FD = p.map(FractalDim,range(nb_stp))
p.close()
vec_FD = np.array(vec_FD)
### Display the evolution of the main root tortuosities:
plt.plot(time0,vec_FD,'-k',linewidth=3)
plt.xlabel('time (min)')
plt.ylabel('main root fractal dimension')
plt.savefig('mainRoot/mainRootFractalDimension.png',dpi=400)
plt.savefig('mainRoot/mainRootFractalDimension.svg')
plt.show()
plt.close()
###Save data:
np.savetxt('mainRoot/fractalDimension.txt',vec_FD)

## Computation of the main root tip direction evolution:
### Make storage vectors:
vec_Dir = np.zeros(len(vec_L))
### Loop over pictures:
for iPct in range(nb_stp):
    #### Load data:
    IJ = np.loadtxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt')
    II = np.where(IJ[:,0]>Imax)[0]
    I0 =- IJ[II,0]
    J0 = IJ[II,1]
    #### Smooth curve:
    ##### Get curvilinear coordinates:
    s = np.arange(len(I0))
    ##### Polynomial smooth of data:
    std = err_pol*np.ones_like(I0)
    I1 = UnivariateSpline(s,I0,k=4,w=1./np.sqrt(std))(s)
    J1 = UnivariateSpline(s,J0,k=4,w=1./np.sqrt(std))(s)
    ####Compute direction:
    Do = -(m.atan(np.polyfit(I1[0:d_dir],J1[0:d_dir],1)[0])+m.pi/2.)
    vec_Dir[iPct] = Do
    
    # !Debug!
    # ~ pict0 = Image.open('picture_processed/'+'%04d'%(iPct)+'.jpg')
    # ~ plt.imshow(pict0)
    # ~ plt.title(str(Do*180/m.pi)+' deg')
    # ~ plt.plot(J0,-I0,'-k')
    # ~ plt.plot(J1,-I1,'-ob')
    # ~ plt.plot(J1[0:d_dir],-I1[0:d_dir],'.r')
    # ~ plt.plot(np.array([J1[0],J1[0]+d_dir*m.cos(Do)]),-np.array([I1[0],I1[0]+d_dir*m.sin(Do)]),'-r')
    # ~ plt.show()
    # ~ plt.close()

###Display the evolution of the main root tip direction:
####1
plt.plot(time0,vec_Dir*180./m.pi,'-k',linewidth=3)
plt.xlabel('time (min)')
plt.ylabel('main root tip direction')
plt.savefig('mainRoot/mainRootTipDirection1.png',dpi=400)
plt.savefig('mainRoot/mainRootTipDirection1.svg')
plt.show()
plt.close()
####2
fig=plt.figure()
ax=fig.add_subplot(111, polar=True)
ax.plot(vec_Dir,time0,'-k',linewidth=3)
plt.savefig('mainRoot/mainRootTipDirection2.png',dpi=400)
plt.savefig('mainRoot/mainRootTipDirection2.svg')
plt.show()
plt.close()
####3
plt.plot(vec_L,vec_Dir*180./m.pi,'-k',linewidth=3)
plt.xlabel('root length (px)')
plt.ylabel('main root tip direction')
plt.savefig('mainRoot/mainRootTipDirection3.png',dpi=400)
plt.savefig('mainRoot/mainRootTipDirection3.svg')
plt.show()
plt.close()
####4
fig=plt.figure()
ax=fig.add_subplot(111, polar=True)
ax.plot(vec_Dir,vec_L,'-k',linewidth=3)
plt.savefig('mainRoot/mainRootTipDirection4.png',dpi=400)
plt.savefig('mainRoot/mainRootTipDirection4.svg')
plt.show()
plt.close()
###Save data:
np.savetxt('mainRoot/direction.txt',vec_Dir)

#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
#Combined view:

##Initialisation:
matDisp = float('nan')*np.ones((len(time0),N0))
os.mkdir('mainRoot/tmp0')
## Loop of frames:
for iPct in range(nb_stp):
    print(str(iPct)+' / '+str(nb_stp))
    ### Load data:
    #### time:
    t0 = time0[iPct]
    #### Mask:
    M0 = np.array(Image.open('pictureSkeleton/mask/'+'%04d'%(iPct)+'.png'))/255.
    #### Root position:
    IJ = np.loadtxt('mainRoot/rootPath/'+str(iPct).zfill(4)+'.txt')
    II = np.where(IJ[:,0]>Imax)[0]
    I0 = IJ[II,0]
    J0 = IJ[II,1]
    if iPct==0:
        Im = int(0.9*np.min(I0))
    
    #### Root curvature:
    C0 = np.loadtxt('mainRoot/rootCurvature/'+str(iPct).zfill(4)+'.txt')
    #### Root tip:
    It = I0[0]; Jt = J0[0]
    #### Root direction:
    D0 = vec_Dir[iPct]
    
    ### Increment:
    C1 = C0[np.arange(len(C0)-1,0,-1)]
    matDisp[-1-iPct,0:len(C1)] = C1
    
    ### Plot:
    fig = plt.figure(1)
    plt.rcParams.update({'font.size':8})
    #### Mask:
    plt.subplot2grid((3,3),(0,0),colspan=2,rowspan=2)
    plt.imshow(M0,cmap='gray',vmin=0,vmax=1)
    sc = plt.scatter(J0,I0,c=np.abs(C0),s=0.5,cmap=plt.cm.get_cmap('jet'),vmin=0,vmax=max(CM,-Cm))
    plt.colorbar(sc)
    plt.plot([Jt],[It],'or',markersize=5)
    plt.plot([Jt,Jt+amp_dir*m.cos(D0)],[It,It-amp_dir*m.sin(D0)],'-r',linewidth=3)
    plt.axis('equal')
    plt.axis('off')
    plt.ylim(M0.shape[0],Im)
    
    #### Time:
    if t0<60:
        plt.annotate(str(int(t0))+' m',(50,100),color='red',fontsize=9,weight='demibold')
        plt.title('root curvature (1/px) \n 00 : '+str(int(t0)).zfill(2)+' - '+str(iPct).zfill(4))
    else:
        h0 = int(t0/60.)
        m0 = int(t0-60.*h0)
        plt.title('root curvature (1/px) \n'+str(h0).zfill(2)+' : '+str(m0).zfill(2)+' - '+str(iPct).zfill(4))
    
    #### Curvature:
    ax1 = plt.subplot2grid((3,3),(0,2))
    plt.imshow(matDisp,vmin=Cm,vmax=CM,cmap=plt.cm.get_cmap('jet'),aspect='auto')
    plt.colorbar()
    plt.title('root curvature (1/px)')
    plt.xlabel('curvilinear coordinate (px)')
    plt.ylabel('time (h)')
    ax1.set_yticks([0,0.33*matDisp.shape[0],0.66*matDisp.shape[0],matDisp.shape[0]])
    ax1.set_yticklabels([str(int(time0[-1]/60.)),str(int(time0[int(len(time0)*0.66)]/60.)),str(int(time0[int(len(time0)*0.33)]/60.)),'0'])
    #### Curvature:
    ax1 = plt.subplot2grid((3,3),(1,2))
    color = 'tab:red'
    ax1.set_xlabel('time (min)')
    ax1.set_ylabel('length (px)',color=color)
    ax1.set_xlim(0,np.max(time0))
    ax1.set_ylim(np.min(vec_L),np.max(vec_L))
    ax1.plot(time0[0:iPct],vec_L[0:iPct],color=color)
    ax1.tick_params(axis='y',labelcolor=color)
    ax2 = ax1.twinx()
    color = 'tab:blue'
    ax2.set_ylabel('speed (px/min)',color=color)
    ax2.plot(time0[0:iPct],vec_V[0:iPct],color=color)
    ax2.tick_params(axis='y',labelcolor=color)
    ax2.set_ylim(np.min(vec_V),np.max(vec_V))
    #### Tortuosity:
    ax1 = plt.subplot2grid((3,3),(2,0),colspan=2)
    color = 'tab:red'
    ax1.set_xlabel('time (min)')
    ax1.set_ylabel('tortuosity',color=color)
    ax1.set_xlim(0,np.max(time0))
    ax1.set_ylim(np.min(vec_Tor1),np.max(vec_Tor1))
    ax1.plot(time0[0:iPct],vec_Tor1[0:iPct],color=color)
    ax1.tick_params(axis='y',labelcolor=color)
    ax2 = ax1.twinx()
    color = 'tab:blue'
    ax2.set_ylabel('fractal dim.',color=color)
    ax2.plot(time0[0:iPct],vec_FD[0:iPct],color=color)
    ax2.tick_params(axis='y',labelcolor=color)
    ax2.set_ylim(np.min(vec_FD),np.max(vec_FD))
    #### Root tip direction:
    ax1 = plt.subplot2grid((3,3),(2,2),polar=True)
    ax1.plot(vec_Dir[0:iPct],time0[0:iPct],'-k',linewidth=3)
    ax1.set_ylim(0.,np.max(time0))
    ### Save plot:
    fig.tight_layout()
    plt.savefig('mainRoot/tmp0/'+str(iPct).zfill(4)+'.png',dpi=200)
    plt.close()

##Make movies: 
null = os.system('ffmpeg -r '+str(fps_mov)+' -y -f image2 -i mainRoot/tmp0/%04d.png -qscale 1 mainRoot/MainRoot.avi')

##Remove folders:
null=os.system('rm -f -R mainRoot/tmp0')















