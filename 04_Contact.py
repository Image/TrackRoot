#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
#Load libraries:

import io
import yaml
import numpy as np
import math as m
from PIL import Image
from scipy import signal as sg
import os
import matplotlib.pyplot as plt
from matplotlib.path import Path
import cv2
from cv2 import Canny

#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~# 
# Input parameter loading:

## Load dictionnary:
with io.open('input_parameters.yaml', 'r') as infile:
    data_input = yaml.load(infile, Loader=yaml.FullLoader)

## Number of frame per second for movies:
fps_mov = data_input['displaying']['frame per second']
## Number of steps:
nb_stp = data_input['general']['number of step']
##Contact detection threshold:
cnt_th = data_input['contact']['contact detection front']
##Radius area considered to compute contact direction:
cnt_rad = data_input['contact']['radius contact']
##Contact detection threshold for root back:
cnt_th_b = data_input['contact']['contact detection back']
## Distance over wich to compute root tip direction:
d_dir = data_input['main root']['distance direction']


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Get contour of already found obstacles:

## Load obstacle map:
map_obst = np.array(Image.open('obstacle/obstacleMask.png'))

### Loop around contour
obst_list = []
for it_obst in range(map_obst.max()):
    #### Make mask:
    mask_obst_cur = np.zeros(map_obst.shape)
    mask_obst_cur[np.where(map_obst==it_obst+1)]=1
    mask_obst_cur = mask_obst_cur.astype('uint8')
    #### Get contour:
    cont_cur,h = cv2.findContours(mask_obst_cur,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    cont_cur = cont_cur[0]
    x_cur = cont_cur[:,0,0].astype('float')
    y_cur = cont_cur[:,0,1].astype('float')
    #### Close the contour:
    x_cur = np.append(x_cur,x_cur[0])
    y_cur = np.append(y_cur,y_cur[0])
    
    #!Debug!
    # ~ plt.imshow(mask_obst_cur)
    # ~ plt.plot(x_cur,y_cur,'-o')
    # ~ plt.show()
    
    #### Store it:
    obst_list.append(np.vstack((x_cur,y_cur)).T)


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Get missing obstacles position on the picture:

## Load the picture on which obstacle are specified:
pict0 = np.array(Image.open('picture_processed/'+'%04d'%(0)+'.jpg'))

## Select obstacles:
### Define a class to draw obstacles:
class LineBuilder:
    def __init__(self, line):
        self.line=line
        self.xs = list(line.get_xdata())
        self.ys = list(line.get_ydata())
        self.cid = line.figure.canvas.mpl_connect('button_press_event', self)
        self.flag = True
    
    def __call__(self,event):
        print('click',event)
        if event.button==3 and event.inaxes:
            if event.inaxes!=self.line.axes: return
            if self.flag:
                self.xs = [event.xdata]
                self.ys = [event.ydata]
                self.flag = False
            else:
                self.xs.append(event.xdata)
                self.ys.append(event.ydata)
                self.line.set_data(self.xs, self.ys)
                self.line.figure.canvas.draw()

### Loop over obstacles:
while True:
    #### Select an obstacle or close to finish:
    fig = plt.figure()
    mng = plt.get_current_fig_manager()
    ax = fig.add_subplot(111)
    ax.imshow(pict0)
    for it_l in range(len(obst_list)):
        plt.plot(obst_list[it_l][:,0],obst_list[it_l][:,1],'-b',linewidth=1)
    
    ax.set_title('Add missing obstacle (if any)\n Click around an obstacles clicks\n -right click to select, left click to zoom, close to end-')
    line, = ax.plot([0],[0])
    linebuilder = LineBuilder(line)
    plt.show()
    if len(linebuilder.xs)>1:
        #### Store the obstacle border:
        x_cur = np.array(linebuilder.xs); x_cur = np.append(x_cur,x_cur[0])
        y_cur = np.array(linebuilder.ys); y_cur = np.append(y_cur,y_cur[0])
        obst_list.append(np.vstack((x_cur,y_cur)).T)
    else:
        ####Close:
        break

### Display obstacles:
#### Make storage folder:
os.system('mkdir obstacle')
#### Create figure:
plt.imshow(pict0)
for it_l in range(len(obst_list)):
    plt.plot(obst_list[it_l][:,0],obst_list[it_l][:,1],'-',linewidth=1)
    plt.text(np.mean(obst_list[it_l][:,0]),np.mean(obst_list[it_l][:,1]),str(it_l+1),fontsize=8)

plt.axis('equal')
plt.axis('off')
plt.savefig('obstacle/numbering.png',bbox_inches='tight',dpi=200)
plt.close()

## Save obstacle coordinates:
mat_save = []
for it_l in range(len(obst_list)):
    for it_m in range(len(obst_list[it_l][:,0])):
        mat_save.append(np.array([it_l+1,obst_list[it_l][it_m,0],obst_list[it_l][it_m,0]]))

np.savetxt('obstacle/coordinate.txt',np.vstack(mat_save))

## Make obstacle mask:
### Initialise mask:
obst_mask = np.zeros((pict0.shape[0],pict0.shape[1]))
I0,J0 = np.meshgrid(np.arange(obst_mask.shape[0]),np.arange(obst_mask.shape[1]))
I0 = I0.flatten(); J0 = J0.flatten()
pict_point = np.vstack((J0,I0)).T 
### Loop over obstacles:
for it_ob in range(len(obst_list)):
    #### Load the current obstacle
    obst_c = obst_list[it_ob]
    #### Make the path:
    path_c = Path(obst_c)
    #### Get points inside:
    grid_c = path_c.contains_points(pict_point)
    #### Update the mask:
    mask_c = grid_c.reshape(pict0.shape[1],pict0.shape[0]).T
    obst_mask += (it_ob+1)*mask_c

### Save the mask:
im = Image.fromarray(obst_mask).convert('L')
im.save('obstacle/bulkMask.png')

## Get obstacle edges:
### Get contour:
edge_raw = Canny(obst_mask.astype('uint8'),1,1)
### Number edges:
edge_raw = sg.convolve2d(edge_raw,np.ones((2,2)),mode='same')>100
obst_edge = edge_raw*obst_mask
### Save it:
im = Image.fromarray(obst_edge).convert('L')
im.save('obstacle/edgeMask.png')


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Detect contact between main root tip and obstacles:

## Load obstacle:
obst_edge = np.array(Image.open('obstacle/edgeMask.png'))
obst_edge = np.round(obst_edge/np.sort(np.unique(obst_edge))[1])

## Load time and position of the tip:
pos_tip = np.loadtxt('mainRoot/rootTip/position_smoothed.txt')
time_tip = np.loadtxt('mainRoot/rootTip/time_smoothed.txt')

## Load the time and root tip direction:
dir0 = np.loadtxt('mainRoot/direction.txt')
time0 = np.loadtxt('time[min].txt')

## Interpolate direction:
dir_tip = np.interp(time_tip,time0,dir0)

## Get contact, obstacle number and contact direction:
### Initialise storage vectors:
cnt_num = np.zeros(len(pos_tip))
cnt_dir = -1.*np.ones(len(pos_tip))
### Prepare obstacle:
I_o,J_o = np.where(obst_edge>0)
N_o = obst_edge[I_o,J_o]
### Loop over positions:
for it_t in range(len(time_tip)):
    #### Get current position:
    Jc,Ic = pos_tip[it_t,0],pos_tip[it_t,1],
    #### Get distance to obstacle edges:
    dc = np.sqrt((Ic-I_o)**2.+(Jc-J_o)**2.)
    #### Contact if distance is smaller than thresold:
    if np.min(dc)<cnt_th:
        #### Get obstacle number:
        II = np.where(dc==np.min(dc))[0][0]
        cnt_num[it_t] = N_o[II]
        #### Get contact direction:
        ##### Measure obstacle direction:
        II = np.where(dc<cnt_rad)[0]
        I_d = I_o[II]; J_d = J_o[II]
        Do = -m.atan(np.polyfit(I_d,J_d,1)[0])+m.pi/2.
        ##### Load root tip direction:
        Dc = -dir_tip[it_t]
        ##### Compute and store angle difference:
        diffD = abs(m.atan2(m.sin(Dc-Do),m.cos(Dc-Do)))
        cnt_dir[it_t] = diffD
        
        # !Debug!
        # ~ plt.plot(J_o,I_o,'ok')
        # ~ plt.plot(J_d,I_d,'xg')
        # ~ plt.plot(pos_tip[0:it_t,0],pos_tip[0:it_t,1],'-m')
        # ~ plt.plot(pos_tip[max(0,it_t-10):it_t,0],pos_tip[max(0,it_t-10):it_t,1],'.k')
        # ~ plt.plot(Jc,Ic,'+r')
        # ~ nn=10.
        # ~ plt.plot(np.array([Jc,Jc+nn*m.cos(Dc)]),np.array([Ic,Ic+nn*m.sin(Dc)]),'-r')
        # ~ plt.plot(np.array([Jc,Jc+nn*m.cos(Do)]),np.array([Ic,Ic+nn*m.sin(Do)]),'-g')
        # ~ plt.axis('equal')
        # ~ plt.xlim(Jc-50,Jc+50)
        # ~ plt.ylim(Ic-50,Ic+50)
        # ~ plt.title(str(int(diffD/m.pi*180.)))
        # ~ plt.savefig('tmp/'+str(it_t).zfill(5)+'.png')
        # ~ plt.close()

### Save contact information:
np.savetxt('mainRoot/rootTip/contactObstacle.txt',cnt_num)
np.savetxt('mainRoot/rootTip/contactAngle.txt',cnt_dir)


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Detect contact between main root body and obstacles:

## Load obstacle mask:
obst_edge = np.array(Image.open('obstacle/edgeMask.png'))
obst_edge = np.round(obst_edge/np.sort(np.unique(obst_edge))[1])

## Load time and position of the tip:
time0 = np.loadtxt('time[min].txt')

## Make storage folder:
os.system('mkdir mainRoot/rootContact')

## Get contact and obstacle number:
### Prepare obstacle:
I_o,J_o = np.where(obst_edge>0)
N_o = obst_edge[I_o,J_o]
### Loop over positions:
for it_t in range(len(time0)):
    #### Get current root path:
    Ic_Jc = np.loadtxt('mainRoot/rootPath/'+str(it_t).zfill(4)+'.txt')
    Ic = Ic_Jc[:,0]; Jc = Ic_Jc[:,1]
    #### Get current weighted skeleton:
    skl_cur = np.array(Image.open('pictureSkeleton/weightedSkeleton/'+str(it_t).zfill(4)+'.png'))
    skl_cur = skl_cur/np.sort(np.unique(skl_cur))[1]
    
    ##~DEBUG~##
    # plt.imshow(skl_cur)
    # plt.plot(Jc,Ic,'-r')
    # plt.show()
    # plt.close()
    
    #### Loop along path to detect contact:
    cnt_num = np.zeros(len(Ic))
    for it in range(len(Ic)):
        ##### Measure distance to obstacles:
        d_cur = np.sqrt((Ic[it]-I_o)**2.+(Jc[it]-J_o)**2.)
        ##### If close enough consider the contact:
        
        ##~DEBUG~##
        # plt.imshow(skl_cur)
        # plt.plot(Jc[it],Ic[it],'or')
        # plt.plot(J_o,I_o,'.r')
        # plt.show()
        # plt.close()
        
        if np.min(d_cur)<(skl_cur[int(Ic[it]),int(Jc[it])]+cnt_th_b):
            II=np.where(d_cur==np.min(d_cur))[0][0]
            cnt_num[it] = N_o[II]
    
    #### Save the result:
    np.savetxt('mainRoot/rootContact/'+str(it_t).zfill(4)+'.txt',cnt_num)


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Display results:

## Make temporary storage:
os.system('mkdir mainRoot/tmp_obst')

## Load root tip contact and contact direction:
cnt_num_0 = np.loadtxt('mainRoot/rootTip/contactObstacle.txt')
time_0 = np.loadtxt('mainRoot/rootTip/time_smoothed.txt')
cnt_num0 = np.interp(time0,time_0,cnt_num_0)

## Load root tip directon:
dir0 = np.loadtxt('mainRoot/direction.txt')

## Loop over time step:
for it_t in range(len(time0)):
    ### Load picture:
    pict0 = Image.open('picture_processed/'+str(it_t).zfill(4)+'.jpg')
    ### Load root path:
    Ic_Jc = np.loadtxt('mainRoot/rootPath/'+str(it_t).zfill(4)+'.txt')
    Ic = Ic_Jc[:,0]; Jc = Ic_Jc[:,1]
    ### Load contacts:
    cnt_num = np.loadtxt('mainRoot/rootContact/'+str(it_t).zfill(4)+'.txt')
    ### Plot background:
    plt.imshow(pict0)
    ### Plot obstacles:
    for it_l in range(len(obst_list)):
        plt.plot(obst_list[it_l][:,0],obst_list[it_l][:,1],'-',linewidth=2,color=plt.cm.jet(float(it_l)/len(obst_list)))
    
    ### Plot path:
    plt.plot(Jc,Ic,'-w',linewidth=2)
    ### Plot contacts:
    cnt_num_u = np.unique(cnt_num)
    for it_o in range(len(cnt_num_u)-1):
        N_o_c = int(cnt_num_u[it_o+1])
        II = np.where(cnt_num==N_o_c)[0]
        plt.plot(Jc[II],Ic[II],'.',markersize=5,color=plt.cm.jet(float(N_o_c-1)/len(obst_list)))
    
    ### Plot tip and contact orientation:
    if cnt_num0[it_t]>0:
        Jc = Jc[0]
        Ic = Ic[0]
        Dc = -dir0[it_t]
        plt.plot(Jc,Ic,'+r',color=plt.cm.jet(float(cnt_num0[it_t]-1)/len(obst_list)))
        plt.plot(np.array([Jc,Jc+d_dir*m.cos(Dc)]),np.array([Ic,Ic+d_dir*m.sin(Dc)]),'-',linewidth=2,color=plt.cm.jet(float(cnt_num0[it_t]-1)/len(obst_list)))
    
    plt.axis('equal')
    plt.axis('off')
    t0 = time0[it_t]
    h0 = int(t0/60.)
    m0 = int(t0-60.*h0)
    plt.title(str(h0).zfill(2)+' : '+str(m0).zfill(2)+' - '+str(it_t).zfill(4))
    plt.savefig('mainRoot/tmp_obst/'+str(it_t).zfill(3)+'.png',bbox_inches='tight',dpi=200)
    plt.close()

## Make movies: 
null = os.system('ffmpeg -r '+str(fps_mov)+' -y -f image2 -i mainRoot/tmp_obst/%03d.png -qscale 1 mainRoot/MainRootContact.avi')

## Remove folders:
null = os.system('rm -f -R mainRoot/tmp_obst')


