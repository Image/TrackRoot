#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Load libraries:

import numpy as np
import math as m
import io
import yaml
from scipy import signal as sg
import fnmatch
import os
import cv2
from PIL import Image
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.widgets import RectangleSelector, Slider, Button

#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Input parameter loading:

## Load dictionnary:
with io.open('input_parameters.yaml', 'r') as infile:
    data_input = yaml.load(infile, Loader=yaml.FullLoader)

##Proximity threshold for root tip connection:
prox_conn_th = data_input['root tip']['proximity connect']
##Proximity threshold for root tip detection:
prox_detec_th = data_input['root tip']['proximity detect']
##Dilation of the obstacle mask:
dil_msk = data_input['root tip']['dilation obstacle']
### Minimum area of the root:
min_area_root = data_input['root tip']['minimum root area']
## Root picture smothening intensity:
pict_smth = data_input['root tip']['dilate erode kernel']
## Root contour smothening intensity:
cont_smth = data_input['root tip']['smooth contour']
## Root contour curvature smoothening:
curv_smth = data_input['root tip']['smooth curvature']
## Threshold on the curvature for root tips detection:
curv_th = data_input['root tip']['curvature threshold']
## Root tip speed smoothening intensity:
spd_smth = data_input['root tip']['smooth speed']
## number of steps:
nb_stp = data_input['general']['number of step']
## Number of frame per second for movies:
fps_mov = data_input['displaying']['frame per second']


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Pick initial variable for the image post processing

## Pick an intensity threshold to make mask:
### Function to threshold the picture: 
def PictTh(pict, val_th):
    return pict>val_th

### Initialize figure:
fig = plt.figure()
ax = fig.add_subplot(111)
fig.subplots_adjust(left=0.1, bottom=0.12)
plt.axis('off')
plt.title('select picture intensity threshold for root detection')
### Make default values:
pict0 = np.array(Image.open('picture_processed/'+str(0).zfill(4)+'.jpg').convert('L'))
val_th_0 = (np.amax(pict0)+np.amin(pict0))/2.
### Make initial plot:
ax.imshow(PictTh(pict0,val_th_0),cmap='gray')
### Define selection sliders:
th_slider_ax = fig.add_axes([0.2, 0.08, 0.65, 0.03])
th_slider = Slider(th_slider_ax, 'intensity threshold', 0., 255., valinit=val_th_0)
num_slider_ax = fig.add_axes([0.2, 0.05, 0.65, 0.03])
num_slider = Slider(num_slider_ax, 'picture number', 0, nb_stp, valinit=0)
### Function happening when values are changed: 
def th_slider_on_changed(val):
    global pict0
    ax.imshow(PictTh(pict0,th_slider.val),cmap='gray')
    plt.draw()

def num_slider_on_changed(val):
    global pict0
    pict0=np.array(Image.open('picture_processed/'+str(int(num_slider.val)).zfill(4)+'.jpg').convert('L')) 
    ax.imshow(PictTh(pict0,th_slider.val),cmap='gray')
    plt.draw()

th_slider.on_changed(th_slider_on_changed)
num_slider.on_changed(num_slider_on_changed)
### Select threshold value:
plt.show()
plt.close()

## Save threshold in yaml file:
pict_th = int(th_slider.val)
data_input['root tip'].update({'picture threshold':pict_th})
with io.open('input_parameters.yaml', 'w', encoding='utf8') as outfile:
    yaml.dump(data_input, outfile, default_flow_style=False, allow_unicode=True)

## Select the areas not to consider to follow the root:   Possibility to slect a polygon???
### Define selection and validation functions:
def line_select_callback(eclick, erelease):
    global x1, y1, x2, y2
    x1,y1 = eclick.xdata, eclick.ydata
    x2,y2 = erelease.xdata, erelease.ydata

def toggle_selector(event):
    print(' Key pressed.')
    if event.key in ['Q', 'q'] and toggle_selector.RS.active:
        print(' RectangleSelector deactivated.')
        toggle_selector.RS.set_active(False)
    if event.key in ['A', 'a'] and not toggle_selector.RS.active:
        print(' RectangleSelector activated.')
        toggle_selector.RS.set_active(True)

def num_slider_on_changed(val):
    global pict1, pict_mask
    pict1 = np.array(Image.open('picture_processed/'+str(int(num_slider.val)).zfill(4)+'.jpg').convert('L')) 
    ax.imshow(pict1*pict_mask,cmap='gray')
    plt.draw()

def mask_button_on_clicked(mouse_event):
    global x1, y1, x2, y2, pict1, pict_mask
    pict_mask[int(y1):int(y2),int(x1):int(x2)] = 0
    ax.imshow(pict1*pict_mask,cmap='gray')
    plt.draw()

### Stepwise selection of the ROI:
#### Intialize plot:
fig = plt.figure()
ax = fig.add_subplot(111)
fig.subplots_adjust(left=0.1, bottom=0.12)
plt.axis('off')
plt.title("select area not to consider for the image processing, then click 'mask'\n close when done")
#### Make default values:
pict1 = np.array(Image.open('picture_processed/'+str(int(num_slider.val)).zfill(4)+'.jpg').convert('L')) 
pict_mask = np.ones(pict1.shape)
#### Make initial plot:
ax.imshow(pict1,cmap='gray')
toggle_selector.RS = RectangleSelector(ax,line_select_callback,drawtype='box',useblit=True,button=[1, 3],minspanx=5,minspany=5,spancoords='pixels',interactive=True)
plt.connect('key_press_event',toggle_selector)
####Define selection sliders:
num_slider_ax = fig.add_axes([0.2, 0.1, 0.65, 0.03])
num_slider = Slider(num_slider_ax, 'picture', 0, nb_stp, valinit=0)
mask_button_ax = fig.add_axes([0.5, 0.04, 0.1, 0.04])
mask_button = Button(mask_button_ax, 'mask')
num_slider.on_changed(num_slider_on_changed)
mask_button.on_clicked(mask_button_on_clicked)
####Select:
plt.show()
plt.close()

### Add obstacle mask:
#### Load the mask:
obst_mask = (np.array(Image.open('obstacle/obstacleMask.png'))<1).astype('uint8')
kernel = np.ones((dil_msk,dil_msk),np.uint8) 
obst_mask = cv2.erode(obst_mask,kernel,iterations=1) 
#### Merge masks:
pict_mask = obst_mask*pict_mask

### Save mask:
os.system('mkdir pictureSkeleton')
im = Image.fromarray(pict_mask*255).convert('L')
im.save('pictureSkeleton/ROI_mask.png')

## Select seed:
### Load the last picture:
pict0 = np.array(Image.open('picture_processed/'+str(0).zfill(4)+'.jpg'))[:,:,0] 
### Select:
def onclick(event):
    global x0, y0
    if event.button==3 and event.inaxes:
        x0.append(event.xdata)
        y0.append(event.ydata)
        plt.plot(event.xdata,event.ydata,'b+')
        plt.draw() 

pict1 = pict0>pict_th
pict1 = (pict1.astype(int)-pict_mask)<0
fig,ax = plt.subplots()
ax.imshow(pict1,vmin=0,vmax=1)
ax.set_title('right click on the head of the root, everything above will be cropped out\nleft click to zoom, close to end')
x0 = []; y0 = []
cid=fig.canvas.mpl_connect('button_press_event', onclick)
plt.show()
plt.close()
JIseed = np.array([int(x0[-1]),int(y0[-1])])

## Save seed position:
data_input['root tip'].update({'I seed position':int(JIseed[1]),'J seed position':int(JIseed[0])})
with io.open('input_parameters.yaml', 'w', encoding='utf8') as outfile:
    yaml.dump(data_input, outfile, default_flow_style=False, allow_unicode=True)


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Extract the mask root network:

## Extract the mask of the root network for all the pictures:
print('Is extracting root mask...')
os.system('mkdir pictureSkeleton/mask')
### Loop over the pictures:
for iPct in range(nb_stp):
    print(str(iPct)+' / '+str(nb_stp))
    #### Load pictures:
    pictA = np.array(Image.open('picture_processed/'+'%04d'%iPct+'.jpg').convert('L'))
    #### Threshold and crop it:
    pictA=(((pictA>pict_th).astype(int)-pict_mask)<0).astype(int)
    pictA[0:int(JIseed[1]),:]=0.
    
    #### Compute the connected areas:
    A = cv2.connectedComponentsWithStats(np.uint8(pictA),4)
    Nb = A[0]
    pictA = A[1]
    areaA = A[2][:,4]
    centA = A[3]
    #### Look for the correct area:
    if iPct==0:
        itArea = 0
        while itArea<=Nb:
            itArea += 1
            ##### Get current pixels
            I,J = np.where(pictA==itArea)
            ##### If current area is big enough to be considered:
            if (len(I)>min_area_root): 
                ##### If the seed point is in the current area this is the root:
                if np.min(np.sqrt((I-JIseed[1])**2.+(J-JIseed[0])**2.))<2:
                    area0 = len(I)
                    cent0 = centA[np.where(area0==areaA)[0],:][0]
                    break
    else:
        ##### Compute the difference of area:
        dArea = np.abs(areaA-area0)
        ##### Compute the distance between centroids:
        dCent = np.sqrt((centA[:,0]-cent0[0])**2.+(centA[:,1]-cent0[1])**2.)
        ##### Select the component with the closest area and centroid:
        I1 = np.argsort(dArea)[0:5]
        I2 = np.argsort(dCent)[0:5]
        I3 = np.intersect1d(I1,I2)
        if len(I3)>0:
            itArea = I3[np.where(dArea[I3]==np.min(dArea[I3]))][0]
        else:
            print('root not properly detected')
        
        cent0=centA[itArea,:]
        area0=areaA[itArea]
    
    ### Extract the root area:
    pictA = pictA==itArea
    ### Save picture:
    im = Image.fromarray(pictA*255.).convert('L')
    im.save('pictureSkeleton/mask/'+'%04d'%iPct+'.png')

print('...extraction done')


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Detection of the root tips:

## Prepare storage folder:
os.system('mkdir rootTip')
os.system('mkdir rootTip/byStep')

## Load time:
timePic = np.loadtxt('time[min].txt')

## Loop over the steps:
for iPct in range(nb_stp):  
    ### Load the mask:
    pict4 = cv2.imread('pictureSkeleton/mask/'+str(iPct).zfill(4)+'.png')[:,:,0]
    ### Fill voids inside the root:
    pict5 = pict4.copy()
    h, w = pict4.shape[:2]
    mask = np.zeros((h+2, w+2), np.uint8)
    cv2.floodFill(pict5, mask, (0,0), 255)
    pict6 = pict4 | cv2.bitwise_not(pict5)
    ### Smoothen shape by erosion and dilation:
    kernel = np.ones((pict_smth,pict_smth),np.uint8)
    pict7 = cv2.erode(pict6,kernel,iterations = 1)
    pict8 = cv2.dilate(pict7,kernel,iterations = 1)
    ### Keep main part:
    A = cv2.connectedComponentsWithStats(pict8,4)
    Nb = A[0]
    pictA = A[1]
    areaA = A[2][:,4]
    if Nb>2:
        I = np.where(areaA==np.sort(areaA)[-2])[0][0]
        pict9 = (pictA==I).astype('uint8')
    else:
        pict9 = (pictA==1).astype('uint8')
    
    ### Computation of the mask contour:
    contour,h = cv2.findContours(pict9,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    contour = contour[0]
    ### Computation of the crossproduct direction:
    #### Load the contour coordinate:
    X = contour[:,0,0].astype('float')
    Y = contour[:,0,1].astype('float')
    #### If the root is long enough:
    if (len(X)>5):
        ####Smothen it:
        X0 = np.convolve(X,np.ones(cont_smth)/float(cont_smth),mode='same'); X0[0]=X[0]; X0[1]=X[1]; X0[-1]=X[-1]; X0[-2]=X[-2]
        Y0 = np.convolve(Y,np.ones(cont_smth)/float(cont_smth),mode='same'); Y0[0]=Y[0]; Y0[1]=Y[1]; Y0[-1]=Y[-1]; Y0[-2]=Y[-2]
        
        #~ # ! Debug !:
        #~ plt.imshow(pict8)
        #~ plt.plot(X,Y,'-r')
        #~ plt.plot(X0,Y0,'-g')
        #~ plt.show()
        
        #### Computation of the cross-product:
        Curv = np.zeros(len(contour))
        for itCurv in range(1,len(contour)-1):
            x0 = X0[itCurv-1]; y0 = Y0[itCurv-1]
            x1 = X0[itCurv];   y1 = Y0[itCurv]
            x2 = X0[itCurv+1]; y2 = Y0[itCurv+1]
            abs1 = m.sqrt((x1-x0)**2+(y1-y0)**2)
            n1x = -(y1-y0)/abs1
            n1y = (x1-x0)/abs1
            abs2 = m.sqrt((x2-x1)**2+(y2-y1)**2)
            n2x = -(y2-y1)/abs2
            n2y = (x2-x1)/abs2
            Curv[itCurv] = n1x*n2y-n1y*n2x
        
        ####Smoothen it:
        Curv0 = np.convolve(Curv,np.ones(curv_smth)/float(curv_smth),mode='same')
        
        # ! Debug !:
        #~ plt.plot(Curv,'-r')
        #~ plt.plot(Curv0,'-g')
        #~ plt.show()
        
        ### Detection of the root tips:  
        #### Extract cross-product extremum and save tip positions:
        I = np.where((Curv0<curv_th))[0]
        if (len(I)>0):
            ##### Detect continuous low curvature areas:
            J = np.unique(np.append([0],np.where(I[0:len(I)-1]-I[1:len(I)]<-2)[0]))
            if (len(J)>1):
                IXtr = np.zeros(len(J))
                for itXtr in range(len(J)-1):
                    IXtr[itXtr] = I[J[itXtr]+np.where(Curv0[I[J[itXtr]+1:J[itXtr+1]+1]]==np.amin(Curv0[I[J[itXtr]+1:J[itXtr+1]+1]]))[0][0]]
                
                IXtr[len(J)-1] = I[J[itXtr+1]+np.where(Curv0[I[J[itXtr+1]+1:len(I)]]==np.amin(Curv0[I[J[itXtr+1]+1:len(I)]]))[0][0]]
                IXtr=IXtr.astype(int)
            else:
                IXtr = np.array([I[np.where(Curv0[I]==np.amin(Curv0[I]))[0][0]]])
            
            #####Extract root tip coordinates:
            vecTip = np.zeros([1,2])
            for itXtr in range(len(IXtr)):
                vecTip = np.append(vecTip,[np.array([X0[IXtr[itXtr]],Y0[IXtr[itXtr]]])],axis=0)
            
            vecTip=np.delete(vecTip,0,0)
            ##### Remove multi detection:
            if vecTip.shape[0]>1:
                vecD = np.sqrt((vecTip[0:-1,0]-vecTip[1:,0])**2.+(vecTip[0:-1,1]-vecTip[1:,1])**2.)
                I = np.where(vecD<prox_detec_th)
                for itI in I:
                    vecTip = np.delete(vecTip,itI,axis=0)
            
            #####Save data:
            np.savetxt('rootTip/byStep/'+'%04d'%iPct+'.txt',vecTip,delimiter=' ')
            
        else:
            vecTip = np.zeros(2)
            np.savetxt('rootTip/byStep/'+'%04d'%iPct+'.txt',vecTip,delimiter=' ')
    else:
        print('root tip not properly detected: root too short')

## Plot and save root tip pathes in color time evolution:
### Loop over the steps:
X = []; Y = []; T = []
for iPct in range(nb_stp):
    if os.path.isfile('rootTip/byStep/'+'%04d'%iPct+'.txt'): 
        XY_tmp = np.loadtxt('rootTip/byStep/'+'%04d'%iPct+'.txt')
        if XY_tmp.ndim>1: 
            X.append(XY_tmp[:,0])
            Y.append(XY_tmp[:,1])
            T.append(timePic[iPct]*np.ones(XY_tmp.shape[0]))
        else:
            if XY_tmp[0]!=0:
                X.append(np.array(XY_tmp[0]))
                Y.append(np.array(XY_tmp[1]))
                T.append(np.array(timePic[iPct]))

### Make position vector:
X0 = np.hstack(X)
Y0 = np.hstack(Y)
T0 = np.hstack(T)

### Plot image and tip position:
pict4 = Image.open('pictureSkeleton/mask/'+'%04d'%(nb_stp-1)+'.png')
plt.imshow(pict4,plt.cm.Greys)
sc = plt.scatter(X0,Y0,c=T0,s=5,cmap=plt.cm.get_cmap('jet'))
plt.colorbar(sc)
plt.title('root tip position in time [min]')
plt.savefig('rootTip/rootTipTime.png',dpi=400)
plt.savefig('rootTip/rootTipTime.svg')
plt.close()


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Tracking of the root tips:

## Prepare storage folder:
os.system('mkdir rootTip/byTip')

### Loop over the steps
for iPct in range(nb_stp): 
    ### Load current tips position vector:
    A = np.loadtxt('rootTip/byStep/'+'%04d'%iPct+'.txt')
    if (A.ndim>0):
        #### Load the positions:
        if (A.ndim==1):
            Xc = np.array([A[0]]); Yc = np.array([A[1]])
        else:
            Xc = A[:,0]; Yc = A[:,1]
        
        #### Check that root tip have been properly detected:
        if Xc[0]!=0:
            #### Load the time:
            tc = timePic[iPct]
            if (iPct==0):
                ### Initialize storage matrix:
                for iPt in range(len(Xc)):
                    if (iPt==0):
                        MatRoot = [np.array([Xc[iPt],Yc[iPt],tc])]
                    else:
                        MatRoot = MatRoot+[np.array([Xc[iPt],Yc[iPt],tc])]
            else:
                ### Track roots:
                while (len(Xc)>0):
                    ####G et current coordinates:
                    xc = Xc[0]; yc = Yc[0]
                    #### Remove them from storage vector:
                    Xc = np.delete(Xc,0); Yc = np.delete(Yc,0)
                    #### Measure the distance to the previously detected tips:
                    Dc = np.zeros(len(MatRoot))
                    for iPt in range(len(MatRoot)):
                        ##### Load the last position:
                        if (MatRoot[iPt].ndim==1):
                            xcc = MatRoot[iPt][0]; ycc = MatRoot[iPt][1]
                        else:
                            xcc = MatRoot[iPt][-1][0]; ycc = MatRoot[iPt][-1][1]
                        
                        ##### Measure the distance:
                        Dc[iPt] = m.sqrt((xc-xcc)**2.+(yc-ycc)**2.)
                        
                    #### Connect to the closest if close enough or add a new root:
                    iPt0 = np.where(Dc==np.min(Dc))[0][0]
                    if Dc[iPt0]<prox_conn_th:
                        MatRoot[iPt0] = np.vstack((MatRoot[iPt0],np.array([xc,yc,tc])))
                    else:
                        MatRoot = MatRoot+[np.array([xc,yc,tc])]

## Remove artefact (root too short):
### Prepare storage matrices:
MatRoot0 = MatRoot
MatRoot = []
### Loop over detected roots:
for iMrt in range(len(MatRoot0)): 
    if (MatRoot0[iMrt].ndim>1):
        #### Load the current root:
        curRoot=MatRoot0[iMrt]
        #### Compute the root amplitude:
        rtAmp=np.max(np.array([np.max(curRoot[:,0])-np.min(curRoot[:,0]),np.max(curRoot[:,1])-np.min(curRoot[:,1])]))
        #### Keep if the root is long enough:
        if rtAmp>prox_conn_th/2.:
            if (len(MatRoot)==0):
                MatRoot=[curRoot]
            else:
                MatRoot=MatRoot+[MatRoot0[iMrt]]

# ! Debug !:
# ~ for iMrt in range(len(MatRoot)):
    # ~ curRoot=MatRoot0[iMrt]
    # ~ plt.plot(curRoot[:,0],curRoot[:,1],'-')

# ~ plt.show()
# ~ plt.close()

## Computation of the speed:
### Prepare to store the speed amplitude:
Vmin = float("inf"); Vmax = 0.
### Loop over root trajectories:
for iMrt in range(len(MatRoot)):    
    #### Get the current trajectory:
    Xc = MatRoot[iMrt][:,0]; Yc = MatRoot[iMrt][:,1]; Tc = MatRoot[iMrt][:,2]
    #### Compute the speed:
    Vc = np.sqrt((Xc[1:len(Xc)]-Xc[0:len(Xc)-1])**2+(Yc[1:len(Yc)]-Yc[0:len(Yc)-1])**2)/(Tc[1:len(Tc)]-Tc[0:len(Tc)-1])
    Vc = np.append(Vc,0.)
    #### Get the speed amplitude:
    if (max(Vc[np.where(Vc<float("inf"))])>Vmax):
        Vmax = max(Vc[np.where(Vc<float("inf"))])
    
    if (min(Vc[np.where(Vc>0)])<Vmin):
        Vmin = min(Vc[np.where(Vc>0)])
    
    #### Smooth data:
    if (len(Vc)>10):
        Vc = np.convolve(Vc,np.ones(spd_smth)/float(spd_smth),mode='same')
    
    #### Store data:
    MatRoot[iMrt] = np.transpose(np.vstack((Xc,Yc,Tc,Vc)))

## Storage of the data:
for iMrt in range(len(MatRoot)): 
    np.savetxt('rootTip/byTip/'+'%04d'%iMrt+'.txt',MatRoot[iMrt],delimiter=' ')

## Plot by speed:
### Prepare for graphical output:
pict4 = Image.open('pictureSkeleton/mask/'+'%04d'%(nb_stp-1)+'.png')
plt.imshow(pict4,plt.cm.Greys)
### Loop over roots:
for iMrt in range(len(MatRoot)):    
    Xc = MatRoot[iMrt][:,0]; Yc = MatRoot[iMrt][:,1]; Tc = MatRoot[iMrt][:,2]; Vc = MatRoot[iMrt][:,3] 
    if iMrt==0:
        sc = plt.scatter(Xc,Yc,c=Vc,s=5,cmap=plt.cm.get_cmap('jet'))
        plt.colorbar(sc)
    else:
        plt.scatter(Xc,Yc,c=Vc,s=5,cmap=plt.cm.get_cmap('jet'))

plt.title('root tip speed')
plt.savefig('rootTip/rootTipSpeed.png',dpi=400)
plt.savefig('rootTip/rootTipSpeed.svg')
plt.close()


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Other graphical outputs:

## Plot the root skeleton with the time: 
timePic=np.loadtxt('time[min].txt')
os.system('mkdir tmp')

print('... is plotting root skeleton')
for iPct in range(nb_stp):
    print(iPct)
    ### Load the time:
    t0 = timePic[iPct]
    h0,m0 = divmod(t0,60)
    j0,h0 = divmod(h0,24)
    ### Load pictures:
    pictA = Image.open('pictureSkeleton/mask/'+'%04d'%iPct+'.png')
    ### Plot picture:
    plt.imshow(pictA,cmap=plt.cm.Greys)
    plt.axis('off')
    plt.title('%02d'%j0+'days   '+'%02d'%h0+'hours  -  '+str(iPct))
    plt.savefig('tmp/'+'%04d'%iPct+'.png',dpi=150)
    plt.close()

## Make movie: 
print('... is making the movie')
os.system('ffmpeg -y -r '+str(fps_mov)+' -f image2 -i tmp/%04d.png -qscale 1 RtSkl.avi')

## Remove folders:
os.system('rm -f -R tmp')

## Space time root network:
os.system('mkdir tmp')
fig = plt.figure()
ax = fig.gca(projection='3d')
for iMrt in range(len(MatRoot)):    
    Xc = MatRoot[iMrt][:,0]; Yc = MatRoot[iMrt][:,1]; Tc = MatRoot[iMrt][:,2];
    ax.plot(Tc/(24*60),Xc,-Yc,'k-',linewidth=3)

plt.title('Root tip positions')
ax.set_xlabel('time (d)')
ax.set_ylabel('x (px)')
ax.set_zlabel('y (px)')
ang = np.linspace(70,70+360,360)
for iPic in range(len(ang)):
    ax.view_init(5,ang[iPic])
    plt.savefig('tmp/'+'%04d'%iPic+'.png',dpi=150)

plt.close()
os.system('ffmpeg -y -r 15 -f image2 -i tmp/%04d.png -qscale 1 TimeSpaceRoot.avi')
os.system('rm -f -R tmp')




