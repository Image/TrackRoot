# This code permits to give the inputs of the image post processing
 
#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Load libraries:

import yaml
import io
from glob import glob

#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Set inputs:

## File organization:
### Name of the image folder [string]:
nam_img = 'picture'
### Image extension [string]:
ext_img = '.jpg'
### Number step to treat [int]:
nb_stp = len(glob(nam_img+'/*'+ext_img))

## Image preprocessing:
### Image rotation [int, degree]:
img_rot = 270
### Need to correct picture shift [bool]:
corr_shift = True

## Displaying:
### Number of frame per second for movies [int]:
fps_mov = 15
### Amplitude of the root tip direction vector [int, px]:
amp_dir = 200 

## Root tip detection and tracking:
### Dilation of the obstacle mask for obstcale removing [odd int, px]:
dil_msk = 7
### Proximity threshold for root tip connection [int, px]:
prox_conn_th = 70
### Proximity threshold for root tip detection [int, px]:
prox_detec_th = 15
### Minimum area of the root [int, px^2]:
min_area_root = 40
### Root icture smothening intensity [int, px]:
pict_smth = 6
### Root contour smothening intensity [int, px]:
cont_smth = 4 
### Root contour curvature smoothening [int, step]:
curv_smth = 10
### Threshold on the curvature for root tips detection [float,<0]:
curv_th = -0.12
### Root tip speed smoothening intensity [int, step]:
spd_smth = 5

## Main root tip analysis:
### Degree of the Chaikin's corner smoothening of the main root tip trajectory [int]:
traj_smth = 3
### Main root tip speed smothening intensity [int, step]:
main_spd_smth = 10
### Mask smoothing dimensions [odd int, px]:
msk_smth = 11
### Root length smoothing intensity [int, step]:
lgth_smth = 10
### Targetted error for the polynomial fitting [float]:
err_pol = 0.2
### Distance over wich to compute root tip direction [int, px]:
d_dir = 20

## Contact detection intputs:
### Contact detection threshold [int, px]:
cnt_th = 12
### Radius area considered to compute contact direction [int, px]:
cnt_rad = 15
### Contact detection threshold for root back [int, px]:
cnt_th_b = 8

## Computational:
### Number of processor for post-processing parallelization:
nb_proc = 6

#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
#Save inputs:


## Make the dictionnary: 
data_input={
'general':               { 'image folder name':        nam_img ,
                           'image extension':          ext_img ,
                           'number of step':           nb_stp} ,
'image preprocessing':   { 'image rotation':           img_rot ,
                           'is shift correction':      corr_shift} ,
'root tip':              { 'dilation obstacle':        dil_msk ,
                           'proximity connect':        prox_conn_th ,
                           'proximity detect':         prox_detec_th ,
                           'minimum root area':        min_area_root ,
                           'dilate erode kernel':      pict_smth ,
                           'smooth contour':           cont_smth ,
                           'smooth curvature':         curv_smth ,
                           'curvature threshold':      curv_th ,
                           'smooth speed':             spd_smth} ,
'main root':             { 'smooth speed':             main_spd_smth ,
                           'trajectory smooth':        traj_smth ,
                           'mask smooth':              msk_smth ,
                           'length smooth':            lgth_smth ,
                           'error polynomial':         err_pol , 
                           'distance direction':       d_dir} ,
'displaying':            { 'frame per second':         fps_mov ,
                           'amplitude direction':      amp_dir } ,
'contact':               { 'contact detection front':  cnt_th ,
                           'contact detection back':   cnt_th_b ,
                           'radius contact':           cnt_rad } ,
'computational':         { 'number processor':         nb_proc }
}

## Save it in a YAML file:
with io.open('input_parameters.yaml', 'w', encoding='utf8') as outfile:
    yaml.dump(data_input, outfile, default_flow_style=False, allow_unicode=True)

