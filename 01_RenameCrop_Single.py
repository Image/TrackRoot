#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Load libraries:

import numpy as np
import os
import io
import glob
import time
import yaml
import datetime
from PIL import Image
from scipy import ndimage as nd
import matplotlib.pyplot as plt
from multiprocessing import Pool
from matplotlib.widgets import RectangleSelector, Slider

#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Input parameter loading:

## Load dictionnary:
with io.open('input_parameters.yaml', 'r') as infile:
    data_input = yaml.load(infile, Loader=yaml.FullLoader)

## Number of processor for post-processing parallelization:
nb_proc = data_input['computational']['number processor']
## Name of the image folder:
nam_img = data_input['general']['image folder name']
## Image extension:
ext_img = data_input['general']['image extension']
## number of steps:
nb_stp = data_input['general']['number of step']
## Image rotation:
img_rot = data_input['image preprocessing']['image rotation']
## Need to correct picture shift:
corr_shift = data_input['image preprocessing']['is shift correction']
## Number of frame per second for movies:
fps_mov = data_input['displaying']['frame per second']

#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Rename file and create time vectors:
print('... is sorting pictures')

## Get picutre names:
nam_pict = glob.glob(nam_img+'/*'+ext_img)

## Initialise time vector:
timePic = np.zeros(nb_stp)

## Create picture vector:
null = os.system('mkdir picture_processed')

## Loop over picture to get their times:
for it in range(len(nam_pict)):
    tmp = Image.open(nam_pict[it])._getexif()[36867]
    dt_tmp = datetime.datetime.strptime(tmp, '%Y:%m:%d %H:%M:%S')
    timePic[it] = time.mktime(dt_tmp.timetuple())
    # timePic[it] = np.float(nam_pict[it][8:-4])

## Sort time vector convert in min and save it: Seconds!!!!
timePic -= np.min(timePic)
Is = np.argsort(timePic)
timePic = timePic[Is]
timePic /= 60.
np.savetxt('time[min].txt',timePic,delimiter='\n')

## Loop over picture to sort and rename them:
for it in range(len(nam_pict)):
    os.system('cp '+nam_pict[Is[it]]+' picture_processed/'+str(it).zfill(4)+'.jpg')

#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Rotate pictures:
print('... is rotating pictures')

## Function to rotate pictures: 
def RotatePicture(iPct):
    print(iPct)
    os.system('convert picture_processed/'+'%04d'%iPct+'.jpg -rotate 270 picture_processed/'+'%04d'%iPct+'.jpg')
    return 0

## Parallelization of the rotation:
if img_rot!=0:
    p = Pool(nb_proc)
    null = p.map(RotatePicture,range(nb_stp))
    p.close()

#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Reframe pictures:

## Get the first picture:
pict1 = np.array(Image.open('picture_processed/'+'%04d'%0+'.jpg'))[:,:,1]
row0,col0 = pict1.shape

## If shift is needed:
if corr_shift:
    ### Define selection functions:
    def line_select_callback(eclick,erelease):
        global x1, y1, x2, y2
        x1,y1 = eclick.xdata,eclick.ydata
        x2,y2 = erelease.xdata,erelease.ydata
    
    def toggle_selector(event):
        print(' Key pressed.')
        if event.key in ['Q', 'q'] and toggle_selector.RS.active:
            print(' RectangleSelector deactivated.')
            toggle_selector.RS.set_active(False)
        if event.key in ['A', 'a'] and not toggle_selector.RS.active:
            print(' RectangleSelector activated.')
            toggle_selector.RS.set_active(True)
    
    ### Select:
    fig,current_ax = plt.subplots()
    plt.imshow(pict1,cmap = plt.cm.Greys)
    plt.title('select area to consider for shift measurement, close to validate')
    toggle_selector.RS = RectangleSelector(current_ax,line_select_callback,drawtype='box',useblit = True,button = [1,3],minspanx = 5,minspany = 5,spancoords = 'pixels',interactive = True)
    plt.connect('key_press_event',toggle_selector)
    plt.show()
    plt.close()
    
    ### Extract picture and compute FFT:
    pict1 = pict1[int(y1):int(y2),int(x1):int(x2)]
    row,col = pict1.shape
    pict1FFT = np.fft.fft2(pict1)
    
    ##Function to measure the shift:
    def MeasurePictureShift(iPct):
        global row, col, pict1FFT
        print(iPct)
        ###Load pictures an select ROI:
        pict2 = np.array(Image.open('picture_processed/'+'%04d'%(iPct)+'.jpg'))[:,:,1]
        pict2 = pict2[int(y1):int(y2),int(x1):int(x2)]
        ###Compute Fourier transform:
        pict2FFT = np.conjugate(np.fft.fft2(pict2))
        ###Convolute:
        pictCCor = np.real(np.fft.ifft2((pict1FFT*pict2FFT)))
        ###Compute the shift: 
        pictCCorShift = np.fft.fftshift(pictCCor)
        yShift,xShift = np.unravel_index(np.argmax(pictCCorShift),(row,col))
        yShift = yShift-int(row/2.)
        xShift = xShift-int(col/2.)
        ###Quantify error:
        a = np.argmax(pictCCorShift)/(row*col)
        
        ###return the shifts:
        return xShift, yShift, a
    
    ## Parallelization of the shift computation:
    print('... is centering pictures (measure displacement)')
    p = Pool(nb_proc)
    xShiftVec0,yShiftVec0,test0 = zip(*p.map(MeasurePictureShift,range(nb_stp)))
    xShiftVec0 = np.array(xShiftVec0); yShiftVec0 = np.array(yShiftVec0); test0 = np.array(test0)
    p.close()
    
    ## Detect picture problems and fix it:
    ### Peak a threshold on image correlation if needed:
    plt.plot(test0,'o',markersize=5)
    plt.ylim(0,np.max(test0) + 2)
    plt.xlabel('step')
    plt.ylabel('correlation value (0-255)')
    plt.title('Select a maximum acceptable value (points higher than selected point will be outliers)') 
    X = plt.ginput(1,timeout=-1)
    plt.close()
    II1 = np.hstack((np.where(test0>X[0][1])[0],10000000))
    ### Peak a threshold on image shift if needed:
    d_test = np.sqrt(xShiftVec0**2.+yShiftVec0**2.)
    plt.plot(d_test,'o',markersize=5)
    plt.ylim(0,np.max(d_test) + 5)
    plt.xlabel('step')
    plt.ylabel('image shift')
    plt.title('Select a maximum acceptable value (points higher than selected point will be outliers)') 
    X = plt.ginput(1,timeout=-1)
    plt.close()
    II2 = np.hstack((np.where(np.sqrt(xShiftVec0**2.+yShiftVec0**2.)>X[0][1])[0],10000000))
    II = np.unique(np.hstack((II1,II2)))
    ### Solve problems if necessary:
    if len(II)>1:
        print('Problem with following pictures: '+str(II)[1:-2])
        III = np.hstack((np.array([-1]),np.where(np.diff(II)>1)[0]))
        for it_III in range(len(III)-1):
            it_ref = II[III[it_III]+1]
            I_mod = np.array(range(it_ref+1,II[III[it_III+1]]+1))
            for it_mod in I_mod:
                os.system('rm -rf picture_processed/'+'%04d'%it_mod+'.jpg')
                os.system('cp picture_processed/'+'%04d'%it_ref+'.jpg picture_processed/'+'%04d'%it_mod+'.jpg')
                os.system('rm -rf picturePl/'+'%04d'%it_mod+'.jpg')
                os.system('cp picturePl/'+'%04d'%it_ref+'.jpg picturePl/'+'%04d'%it_mod+'.jpg')
        
        for it_II in II[0:-1]:
            A0 = MeasurePictureShift(it_II)
            xShiftVec0[it_II] = A0[0]
            yShiftVec0[it_II] = A0[1]
            test0[it_II] = A0[2]
    
    yShiftVec0 = -yShiftVec0
    
    ##Computation of the new image size:
    MaxSft_x = np.max(xShiftVec0)+1
    MinSft_x = np.min(xShiftVec0)-1
    MaxSft_y = np.max(yShiftVec0)+1
    MinSft_y = np.min(yShiftVec0)-1
    colN = int(col0-MaxSft_x+MinSft_x)
    rowN = int(row0-MaxSft_y+MinSft_y)
    
    ##Rescale vectors:
    xShiftVecN = (MaxSft_x-xShiftVec0).astype(int)
    yShiftVecN = (MaxSft_y-yShiftVec0).astype(int)
    
    ##Function to reframe the pictures: 
    def ReframePicture(iPct):
        print(iPct)
        ###Load pictures:
        pict1 = np.array(Image.open('picture_processed/'+'%04d'%(iPct)+'.jpg'))
        ###Crop pictures: 
        pict2 = pict1[:,xShiftVecN[iPct]:xShiftVecN[iPct]+colN,:]
        pict3 = pict2[yShiftVecN[iPct]:yShiftVecN[iPct]+rowN,:,:]
        ###Save picture:
        im = Image.fromarray(pict3)
        im.save('picture_processed/'+'%04d'%iPct+'.jpg')
    
    ##Parallelization of the cropping:
    print('... is centering pictures (crop)')
    p = Pool(nb_proc)
    null = p.map(ReframePicture,range(nb_stp))
    p.close()

## Select the area of interest:
### Open the last picture:
pict1 = np.array(Image.open('picture_processed/'+'%04d'%(nb_stp-1)+'.jpg'))
### Define selection functions:
def line_select_callback(eclick,erelease):
    global x1, y1, x2, y2
    x1,y1=eclick.xdata,eclick.ydata
    x2,y2=erelease.xdata,erelease.ydata

def toggle_selector(event):
    print(' Key pressed.')
    if event.key in ['Q', 'q'] and toggle_selector.RS.active:
        print(' RectangleSelector deactivated.')
        toggle_selector.RS.set_active(False)
    if event.key in ['A', 'a'] and not toggle_selector.RS.active:
        print(' RectangleSelector activated.')
        toggle_selector.RS.set_active(True)

###Select:
fig,current_ax=plt.subplots()
plt.imshow(pict1,cmap=plt.cm.Greys)
plt.title('select area to consider, the rest will be cropped out, close to validate')
toggle_selector.RS=RectangleSelector(current_ax,line_select_callback,drawtype='box',useblit=True,button=[1,3],minspanx=5,minspany=5,spancoords='pixels',interactive=True)
plt.connect('key_press_event',toggle_selector)
plt.show()
plt.close()

## Function to crop the pictures: 
def CropPicture(iPct):
    global x1, y1, x2, y2
    print(iPct)
    ### Load pictures:
    pict1 = np.array(Image.open('picture_processed/'+'%04d'%iPct+'.jpg'))
    ### Crop pictures: 
    pictF1 = pict1[int(y1):int(y2),int(x1):int(x2),:]
    ### Save picture:
    im = Image.fromarray(pictF1)
    im.save('picture_processed/'+'%04d'%iPct+'.jpg')

##Parallelization of the cropping:
print('... is cropping pictures')
p=Pool(nb_proc)
null=p.map(CropPicture,range(nb_stp))
p.close()


#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
# Get obstacles:

## Load the initial picture (converted in black en white):
pict1 = np.array(Image.open('picture_processed/'+'%04d'%0+'.jpg').convert('L'))

## Threshold it:
### Function to threshold the picture: 
def PictTh(pict, val_th, min_siz):
    #### Threshold the picture:
    pict_th = pict<val_th
    #### Get disconnected islands:
    pict_lbl, nb_lbl = nd.measurements.label(pict_th)
    vec_lbl = np.arange(nb_lbl+1)
    #### Get their size:
    siz_lbl = nd.labeled_comprehension(pict_th, pict_lbl, vec_lbl, len, float, 0)
    #### Remove back ground:
    siz_lbl = siz_lbl[1:-1]
    vec_lbl = vec_lbl[1:-1]
    #### Get label of large islands:
    vec_lrg = vec_lbl[np.where(siz_lbl>min_siz)]
    return (np.isin(pict_lbl,vec_lrg), len(vec_lrg))

### Initialize figure:
fig = plt.figure()
ax = fig.add_subplot(111)
fig.subplots_adjust(left=0.1, bottom=0.12)
plt.axis('off')
### Make default values
val_th_0 = np.median(pict1)
min_siz_0 = 0.01*pict1.size
###Make initial plot:
ax.imshow(PictTh(pict1,val_th_0,min_siz_0)[0],cmap='gray')
plt.title('number of obstacles: '+str(PictTh(pict1,val_th_0,min_siz_0)[1]))
###Define selection sliders:
th_slider_ax = fig.add_axes([0.2, 0.08, 0.65, 0.03])
th_slider = Slider(th_slider_ax, 'gray level threshold', 0., 255., valinit=val_th_0)
min_slider_ax = fig.add_axes([0.2, 0.05, 0.65, 0.03])
min_slider = Slider(min_slider_ax, 'mininum obstacle size [px$^2$]', 0., 0.01*pict1.size, valinit=val_th_0)
###Function happening when values are changed: 
def slider_on_changed(val):
    pict2plot,nb_obst = PictTh(pict1,th_slider.val,min_slider.val)
    ax.imshow(pict2plot,cmap='gray')
    ax.set_title('number of obstacles: '+str(nb_obst))
    plt.draw()

th_slider.on_changed(slider_on_changed)
min_slider.on_changed(slider_on_changed)
### Select threshold values:
plt.show()
plt.close()

## Validate results and remove extra objects:
### Get obstacle position:
mask_obst = PictTh(pict1,th_slider.val,min_slider.val)[0]
obst_lbl,nb_obst = nd.measurements.label(mask_obst)
### Loop to remove obstacle:
while True:
    try:
        #### Diplay obstables to select:
        plt.imshow(1-mask_obst,cmap='gray')
        plt.imshow(obst_lbl,cmap='jet',alpha=0.5,vmin=0,vmax=nb_obst)
        for it_obst in range(1,nb_obst+1):
            I,J = np.where(obst_lbl==it_obst)
            plt.text(np.mean(J),np.mean(I),str(it_obst),horizontalalignment='center',color='red')
        
        plt.title('click on obstacle to remove, click on the left to finish to finish')
        plt.xlim(-obst_lbl.shape[1]/3,obst_lbl.shape[1])
        A = plt.ginput(1)
        plt.close()
        if A[0][0]<0:
            break
        else:
            #### Remove select obstable:
            d_obst = np.zeros(nb_obst)
            for it_obst in range(1,nb_obst+1):
                I,J = np.where(obst_lbl==it_obst)
                d_obst[it_obst-1] = np.sqrt((np.mean(J)-A[0][0])**2.+(np.mean(I)-A[0][1])**2.)
            
            II = np.where(d_obst==np.min(d_obst))[0][0]
            obst_lbl[np.where(obst_lbl==II+1)] = 0
            #### Renumber obstacle:
            I_obst = np.sort(np.unique(obst_lbl))
            for it_obst, n_obst in enumerate(I_obst):
                if it_obst>0:
                    obst_lbl[np.where(obst_lbl==n_obst)] = it_obst
            
            nb_obst -= 1
    except:
        break

## Display obstacles:
### Make storage folder:
os.system('mkdir obstacle')
### Create figure:
fig=plt.figure()
plt.imshow(1-mask_obst,cmap='gray')
plt.imshow(obst_lbl,cmap='jet',alpha=0.5,vmin=0,vmax=nb_obst)
for it_obst in range(1,nb_obst+1):
    I,J=np.where(obst_lbl==it_obst)
    plt.text(np.mean(J),np.mean(I),str(it_obst),horizontalalignment='center',color='red')

plt.axis('equal')
plt.axis('off')
plt.savefig('obstacle/numbering.png',bbox_inches='tight',dpi=200)
plt.close()
### Save obstacle map:
im = Image.fromarray(obst_lbl)
im.save('obstacle/obstacleMask.png')

## Save threshold in a YAML file:
data_input.update({'obstacle':{'threshold intensity':int(th_slider.val), 'threshold size':int(min_slider.val)}})
with io.open('input_parameters.yaml', 'w', encoding='utf8') as outfile:
    yaml.dump(data_input, outfile, default_flow_style=False, allow_unicode=True)

#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#~/"\~#
#Create movies:

## Create picture vector:
null=os.system('mkdir tmp_mov')

## Plot the pictures with the time: 
for iPct in range(nb_stp):
    print(iPct)
    ###Load the time:
    t0=timePic[iPct]
    h0,m0 = divmod(t0,60)
    j0,h0 = divmod(h0,24)
    ###Load pictures:
    pict1 = np.array(Image.open('picture_processed/'+'%04d'%iPct+'.jpg'))
    ###Plot picture:
    plt.imshow(pict1)
    plt.axis('off')
    plt.title('%02d'%j0+'days   '+'%02d'%h0+'hours  -  '+str(iPct))
    plt.savefig('tmp_mov/'+'%04d'%iPct+'.png',dpi=200)
    plt.close()

##Make movies: 
print('... is making the movie')
null=os.system('ffmpeg -r '+str(fps_mov)+' -y -f image2 -i tmp_mov/%04d.png -qscale 1 raw_picture.avi')

##Remove folders:
null=os.system('rm -f -R tmp_mov')


















































